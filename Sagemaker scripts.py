import json
import logging
import boto3
import os
from datetime import datetime, date, time
import random
import string

def copyModel(event,context):
    s3 = boto3.resource('s3')
    copy_source = {
    'Bucket': 'rgis-sagemaker-data',
    'Key': 'rgis-predictive-acceptance-estimator/model'+'/'+getTrainingJobs(event,context)+'/'+'output'+'/'+'model.tar.gz'
    }
    s3.meta.client.copy(copy_source, 'rgis-models-dev', 'rgis-predictive-acceptance/'+getTrainingJobs(event,context)+'/'+'model.tar.gz')

def describetrainingjobs(event,context):
    status=[]
    client = boto3.client('sagemaker')
    response = client.describe_training_job(
    TrainingJobName='rgis-predictive-acceptance-estimator-qjqzevbiis'
    )
    obj=json.dumps(response,default=str)
    describetrainingjob=json.loads(obj)
    for statusname in describetrainingjob['SecondaryStatusTransitions']:
        # TODO: write code...
        status.append(statusname['Status'])
    return status

def getTrainingJobs(event,context): 
    job=[]
    client = boto3.client('sagemaker')
    response = client.list_training_jobs()
    obj=json.dumps(response,default=str)
    getjobs=json.loads(obj)
    for jobname in getjobs['TrainingJobSummaries']:
        # TODO: write code...
        job.append(jobname['TrainingJobName'])
    return job[0]
    
def createModel(event,context):
    client = boto3.client('sagemaker')
    response = client.create_model(
    ModelName='model-predictive-acceptance-dev-2-2',
    ExecutionRoleArn='arn:aws:iam::946931270322:role/service-role/AmazonSageMaker-ExecutionRole-20190224T174419',
    Tags=[
        {
            'Key': 'Team',
            'Value': 'Blackstraw'
        }
    ],
    PrimaryContainer=
        {
            'Image': '946931270322.dkr.ecr.us-east-1.amazonaws.com/rgis-predictive-acceptance-estimator:dev',
            'ModelDataUrl': 's3://rgis-models-dev/rgis-predictive-acceptance/dev-2-1/model.tar.gz',
            'Environment': {
            'ENVIRONMENT': 'DEV'
            }
        },
    VpcConfig={
        'SecurityGroupIds': [
            'sg-08fad2661714e1fdd',
            ],
        'Subnets': [
            'subnet-0b6c8a08ec613e60f',
            'subnet-04f6c4de9a588bc5e'
            ]
        }
    )    
    return response

def createEndpointConfig(event,context):
    client = boto3.client('sagemaker')
    response = client.create_endpoint_config(
    EndpointConfigName='endpoint-conf-predictive-acceptance-dev-2-2',
    ProductionVariants=[
        {
            'VariantName': 'variant-name-1',
            'ModelName': 'model-predictive-acceptance-dev-2-1',
            'InitialInstanceCount': 1,
            'InstanceType': 'ml.m4.xlarge',
            'InitialVariantWeight': 1
        }
    ],
    Tags=[
            {
            'Key': 'Team',
            'Value': 'Blackstraw'
            }
        ]
    )
    return response
 
def createEndpoint(event,context):
    client = boto3.client('sagemaker')
    client.create_endpoint(
    EndpointName='endpoint-predictive-acceptance-dev-2-2',
    EndpointConfigName='endpoint-conf-predictive-acceptance-dev-2-2',
    Tags=[
        {
            'Key': 'Team',
            'Value': 'Blackstraw'
        }
    ]
    )
    return response

def randomString():
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(10))
    print ("Random String is ", randomString() )
    
def createjob(event,context):
    client = boto3.client('sagemaker')
    response = client.create_training_job(
        TrainingJobName = 'rgis-predictive-acceptance-estimator-'+randomString(),
        HyperParameters = {
            "categorical_columns":"PERSON_CURRENT_JOB_DESC,ML_ASET_TYPE",
            "features":"AVG_HOURS,AVG_NUMBER_OF_BREAKS,AVG_PER_APH,PERSON_CURRENT_JOB_DESC,PCT_BACKROOM_FLAG,PCT_MSR_FLAG,PCT_NIGHT_SHIFT,ML_ASET_TYPE",
            "job_title_columns":"PERSON_CURRENT_JOB_DESC",
            "non_nan_columns":"PERSON_CURRENT_JOB_DESC:UNKNOWN,HOURS_TOTAL:0,PER_APH_AVG:0,NUMBER_OF_BREAKS_CNT:0,BREAK_VIOLATION_CNT:0,MEAL_VIOLATION_CNT:0",
            "person_id_column":"PERSON_ID",
            "skip_no_show_include": "TRUE",
            "target": "CHANGE_REQUEST_TYPE_CODE",
            "num_round": "10"  
        },
        AlgorithmSpecification={
            "TrainingImage": '946931270322.dkr.ecr.us-east-1.amazonaws.com/rgis-predictive-acceptance-estimator:dev',
            "TrainingInputMode": "File"
        },
        RoleArn = 'arn:aws:iam::946931270322:role/service-role/AmazonSageMaker-ExecutionRole-20190224T174419',
        ResourceConfig = {
            'InstanceCount':1,
            'InstanceType':'ml.m4.xlarge',
            'VolumeSizeInGB':10
        },
        OutputDataConfig ={
            'S3OutputPath': 's3://rgis-sagemaker-data/rgis-predictive-acceptance-estimator/model'
        },
        StoppingCondition= {
            "MaxRuntimeInSeconds": 86400
        },
        InputDataConfig= [
            {
            "ChannelName": "training",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": 's3://rgis-sagemaker-data/rgis-predictive-acceptance-estimator/input/data/training',
                    "S3DataDistributionType": "FullyReplicated" 
                }
            },
            "ContentType": "text/csv",
            "CompressionType": "None"
            }
          ]
        )
    return response    

