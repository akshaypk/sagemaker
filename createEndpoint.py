import sagemaker
import copy
import boto3
import json

client = boto3.client('sagemaker',region_name='us-east-1')

config = []

config=json.loads(open('parameters.json').read())

print(config['ModelName'])  
print(config['Image']) 
print(config['ModelDataUrl'])

response=client.create_endpoint(
    EndpointName='endpoint-'+config['ModelName'],
    EndpointConfigName='endpoint-conf-'+config['ModelName'],
    Tags=[
        {
            'Key': 'Team',
            'Value': 'Blackstraw'
        }
    ]
    )
print(response)