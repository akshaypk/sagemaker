import sagemaker
import copy
import boto3
import json

client = boto3.client('sagemaker',region_name='us-east-1')

config = []

config=json.loads(open('parameters.json').read())

print(config['ModelName'])  
print(config['Image']) 
print(config['ModelDataUrl'])
#Creating Endpoint Config
response = client.create_endpoint_config(
    EndpointConfigName='endpoint-conf-'+config['ModelName'],
    ProductionVariants=[
        {
            'VariantName': 'variant-name-1',
            'ModelName': config['ModelName'],
            'InitialInstanceCount': 1,
            'InstanceType': 'ml.m4.xlarge',
            'InitialVariantWeight': 1
        }
    ],
    Tags=[
            {
            'Key': 'Team',
            'Value': 'Blackstraw'
            }
        ]
    )
print(response)