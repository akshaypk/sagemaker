import sagemaker
import copy
import boto3
import json

from sagemaker.amazon.amazon_estimator import get_image_uri

config = []

config=json.loads(open('parameters.json').read())

print(config['ModelName'])  
print(config['Image']) 
print(config['ModelDataUrl'])

session = boto3.Session(
    region_name="us-east-1"
)

sagemaker_new_session = sagemaker.Session(boto_session=session)
container = config['Image']
train_data = config['TrainData']
role = 'arn:aws:iam::419991610827:role/service-role/AmazonSageMaker-ExecutionRole-20190617T191942'
s3_output_location = config['OutputData']

sage = sagemaker.estimator.Estimator(container,
                                         role, 
                                         train_instance_count=1, 
                                         train_instance_type='ml.m4.xlarge',
                                         train_volume_size = 5,
                                         output_path=s3_output_location,
                                         sagemaker_session=sagemaker_new_session)

train_channel = sagemaker.session.s3_input(train_data, content_type='text/csv')
data_channels = {'train': train_channel}
sage.fit(inputs=data_channels,  logs=True)

