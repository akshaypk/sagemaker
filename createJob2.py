import json
import logging
import boto3
import os
from datetime import datetime, date, time



client = boto3.client('sagemaker',region_name='us-east-1')

config = []

config=json.loads(open('parameters.json').read())

print(config['ModelName'])  
print(config['Image']) 
print(config['ModelDataUrl'])
    
response = client.create_training_job(
        TrainingJobName = config['TrainingJobName']+datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),
        AlgorithmSpecification={
            "TrainingImage": config['Image'],
            "TrainingInputMode": "File"
        },
        RoleArn = 'arn:aws:iam::419991610827:role/Sagemaker-Services-Access',
        ResourceConfig = {
            'InstanceCount':1,
            'InstanceType':'ml.m4.xlarge',
            'VolumeSizeInGB':10
        },
        OutputDataConfig ={
            'S3OutputPath': config['OutputData']
        },
        StoppingCondition= {
            "MaxRuntimeInSeconds": 86400
        },
        InputDataConfig= [
            {
            "ChannelName": "training",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": config['TrainData'],
                    "S3DataDistributionType": "FullyReplicated" 
                }
            },
            "ContentType": "text/csv",
            "CompressionType": "None"
            }
          ]
        )

print(response)
