import sagemaker
import copy
import boto3
import json

client = boto3.client('sagemaker',region_name='us-east-1')


config = []

config=json.loads(open('parameters.json').read())

print(config['ModelName'])  
print(config['Image']) 
print(config['ModelDataUrl'])     

response = client.create_model(
    ModelName=config['ModelName'],
    ExecutionRoleArn='arn:aws:iam::419991610827:role/service-role/AmazonSageMaker-ExecutionRole-20190617T191942',
    Tags=[
        {
            'Key': 'Team',
            'Value': 'Blackstraw'
        }
    ],
    PrimaryContainer=
        {
            'Image': config['Image'],
            'ModelDataUrl': config['ModelDataUrl'],
            'Environment': {
            'ENVIRONMENT': config['Environment_dev']
            }
        },
    VpcConfig={
        'SecurityGroupIds': [
            'sg-654a803c',
            ],
        'Subnets': [
            'subnet-921d5bf5',
            'subnet-9949c3a7'
            ]
        }
    )    
print(response)   